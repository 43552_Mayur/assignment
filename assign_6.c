
#include<stdio.h>
#define MAX 3
struct student
{
	int roll;
	char name[20];
	int std;
	struct subject
	{
		int marks;
	}eng,mar,hin,mat,sci,geo;
};
struct student queue [MAX];
int front=-1, rear=-1;
void push_element(void)
{

	if(front ==0 && rear==MAX-1)
	{
		printf("Queue is FULL\n");
		return;
	}
	else if(front==-1 && rear==-1)
	{
		front =rear=0;
	}
	else if(rear==MAX-1 && front!=0 )
		rear=0;
	else
		rear++;
	printf("Enter roll	:	");
		scanf("%d",&queue[rear].roll);
		printf("Enter name	:	");
		scanf("%s",queue[rear].name);
		printf("Enter std	:	");
		scanf("%d",&queue[rear].std);
		printf("Enter following subject marks:\n");
		printf("English	:	");
		scanf("%d",&queue[rear].eng.marks);
		printf("Marathi	:	");
		scanf("%d",&queue[rear].mar.marks);
		printf("Hindi	:	");
		scanf("%d",&queue[rear].hin.marks);
		printf("Maths	:	");
		scanf("%d",&queue[rear].mat.marks);
		printf("Science	:	");
		scanf("%d",&queue[rear].sci.marks);
		printf("Geo	:	");
		scanf("%d",&queue[rear].geo.marks);

}
int pop_element(void)
{
	if(front==-1 && rear==-1)
	{
		printf("Queue is EMPTY\n");
		return -1;
	}
	int temp=queue[front].roll;
	if(front==rear)
	{
		front=rear=-1;
	}
	else
	{
		if(front==MAX-1)
			front=0;
		else
			front++;
	}
	return temp;
}
void print_record(void)
{
	int rear_posn = rear;
	int front_posn = front;

	if(front==-1)
	{
		printf("\nqueue is empty\n");
		return;
	}
		if (front_posn <= rear_posn)
		while (front_posn <= rear_posn)
		{
				printf("Roll	:	%d\n",queue[front_posn].roll);
				printf("Name	:	%s\n",queue[front_posn].name);
				printf("Std	:	%d\n",queue[front_posn].std);
				printf("Marks:\n");
				printf("English	:	%d\n",queue[front_posn].eng.marks);
				printf("Hindi	:	%d\n",queue[front_posn].hin.marks);
				printf("Marathi	:	%d\n",queue[front_posn].mar.marks);
				printf("Maths	:	%d\n",queue[front_posn].mat.marks);
				printf("Science	:	%d\n",queue[front_posn].sci.marks);
				printf("Geo	:	%d\n",queue[front_posn].geo.marks);
				printf("\n\n");
				front_posn++;
		}
		else
		{
			while(front_posn == MAX - 1)
			{
				printf("Roll	:	%d\n",queue[front_posn].roll);
				printf("Name	:	%s\n",queue[front_posn].name);
				printf("Std	:	%d\n",queue[front_posn].std);
				printf("Marks:\n");
				printf("English	:	%d\n",queue[front_posn].eng.marks);
				printf("Hindi	:	%d\n",queue[front_posn].hin.marks);
				printf("Marathi	:	%d\n",queue[front_posn].mar.marks);
				printf("Maths	:	%d\n",queue[front_posn].mat.marks);
				printf("Science	:	%d\n",queue[front_posn].sci.marks);
				printf("Geo	:	%d\n",queue[front_posn].geo.marks);
				printf("\n\n");
				front_posn++;
			}
			front_posn == 0;
			while (front_posn <= rear_posn)
			{
				printf("Roll	:	%d\n",queue[front_posn].roll);
				printf("Name	:	%s\n",queue[front_posn].name);
				printf("Std	:	%d\n",queue[front_posn].std);
				printf("Marks:\n");
				printf("English	:	%d\n",queue[front_posn].eng.marks);
				printf("Hindi	:	%d\n",queue[front_posn].hin.marks);
				printf("Marathi	:	%d\n",queue[front_posn].mar.marks);
				printf("Maths	:	%d\n",queue[front_posn].mat.marks);
				printf("Science	:	%d\n",queue[front_posn].sci.marks);
				printf("Geo	:	%d\n",queue[front_posn].geo.marks);
				printf("\n\n");
				front_posn++;
				}
		}
		}
int menu_choice(void)
{
	int ch;
	printf("0. Exit\n");
	printf("1. Add element\n");
	printf("2. Remove element\n");
	printf("3. Display element\n");
	printf("Enter your choice	:	");
	scanf("%d",&ch);
	return ch;
}
int main (void)
{
	int ch,temp;
	while((ch=menu_choice())!=0)
	{
		switch(ch)
		{
		case 1:
			push_element();
			break;
		case 2:
			temp=pop_element();
			if(temp!=-1)
			printf("Record of roll no. %d deleted\n",temp);
			break;
		case 3:
			print_record();
			break;
		}
	}

	return 0;
}

