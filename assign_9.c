#include<stdio.h>
#include <string.h>
#include <stdlib.h>


#define PRODUCT_DB  "product.db"
#define TMP_DB      "tmp.db"
#define ORDER_DB    "order.db"

typedef struct inventory {
	int id;
	char name[30];
	double price;
	int qty;
}inventory_t;

typedef struct date {
	int day;
	int month;
	int year;
}date_t;

void date_accept(date_t *d) {
	printf("date: ");
	scanf("%d%d%d", &d->day, &d->month, &d->year);
}

void date_print(date_t *d) {
	printf("date: %d-%d-%d\n", d->day, d->month, d->year);
}

typedef struct order{
    int id;
    char name[50];
    int quantity;
	double price;
    date_t receive_date;
	date_t delivery_date;
}order_t;

void order_accept(order_t *o)
{
	o->id = get_next_order_id();
	printf("Enter Customer name :");
	scanf("%s",o->name);
	printf("Enter quantity :");
	scanf("%d",&o->quantity);
	printf("Enter Order price :");
	scanf("%lf",&o->price);
	 printf("issue date ");
     date_accept(&o->receive_date);
	 printf("delivery date :");
	 date_accept(&o->delivery_date);
}

void order_display(order_t *o){
        printf("Order id: %d, Name : %s, Quantity: %d, price: %.2lf\n",o->id, o->name, o->quantity, o->price);
        printf("issue ");
        date_print(&o->receive_date);
        printf("delivery ");
        date_print(&o->delivery_date);
}

void user_accept(inventory_t *u)
{
    // printf("User id  :");
    // scanf("%d",&u->item_id);
    u->id = get_next_product_id();
    printf("product name    :");
    scanf("%s",u->name);
    printf("price  :");
    scanf("%lf",&u->price);
    printf("Enter Quantity  :");
    scanf("%d",&u->qty);
}

void product_display(inventory_t *u)
{
    printf("product_id : %d, product_Name : %s, Price : %.2lf, Quantity : %d\n",u->id,u->name,u->price,u->qty);
}

// case 1 (sub_part) 
int get_next_product_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(inventory_t);
	inventory_t u;
	
	fp = fopen(PRODUCT_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		
		max = u.id;
	
	fclose(fp);
	
	return max + 1;
}

    //  CASE-1
void add_product() {
	FILE *fp;
	// input item details
	inventory_t p;
     user_accept(&p);
	
	p.id = get_next_product_id();
	
	fp = fopen(PRODUCT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
	// append book to the file.
	fwrite(&p, sizeof(inventory_t), 1, fp);
	printf("book added in file.\n");
	
	fclose(fp);
}
// CASE-2
void product_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	inventory_t b;
	
	fp = fopen(PRODUCT_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	while(fread(&b, sizeof(inventory_t), 1, fp) > 0) {
		
		if(strstr(b.name, name) != NULL) {
			found = 1;
			product_display(&b);
		}
	}
	
	fclose(fp);
	if(!found)
		printf("No such product found.\n");
}
// CASE-3
void display_all_product(){
	FILE *fp;
    inventory_t u;
	fp = fopen(PRODUCT_DB, "rb");
	if(fp == NULL)
	{
		perror("Cannot open file \n");
		exit(0);
	}
	while(fread(&u, sizeof(inventory_t), 1, fp) > 0)
	 {
		product_display(&u);
	 }
	fclose(fp);
}

// CASE-4
void edit_product_by_id() {

	int found = 0;
	int id;
	FILE *fp;
    inventory_t m;

	printf("Enter product id :");
	scanf("%d",&id);
	fp = fopen(PRODUCT_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open file");
		exit(1);
	}
	while(fread(&m, sizeof(inventory_t), 1, fp) > 0) {
		
		if(id == m.id) {
			found = 1;
			break;
		}
	}

	if(found) {
	
		long size = sizeof(inventory_t);   // input NEW details from user
		printf("Edit product Name  :");
		scanf("%s", m.name);
        printf("Edit price :");
		scanf("%lf", &m.price);
        printf("Edit Quantity :");
		scanf("%d", &m.qty);

		fseek(fp, -size, SEEK_CUR);       // take file position one record behind.
		fwrite(&m, size, 1, fp);          // overwrite details into the file
		printf("product updated successfully.\n");
	}
	else 
		printf("not found.\n");
	
	fclose(fp);
}

// CASE-5
void delete_operation(){
	int id;
	FILE *fp;
	FILE *fp_tmp;
	inventory_t data;
	printf("Enter product id :");
	scanf("%d",&id);
    

	fp=fopen(PRODUCT_DB, "rb");
	fp_tmp=fopen(TMP_DB, "wb");
		rewind(fp);
		
		while(fread(&data,sizeof(inventory_t), 1, fp) > 0)
		 {
			if(data.id != id)
			{
				fwrite(&data, sizeof(inventory_t),1,fp_tmp);
			}
		 }
		 fclose(fp_tmp);
		 fclose(fp);
		 remove(PRODUCT_DB);
		 rename(TMP_DB,PRODUCT_DB);
		 printf("\nRecord deleted");
}

//	CASE-6 (sub program)
int get_next_order_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(order_t);
	order_t u;
	
	fp = fopen(ORDER_DB, "rb");
	if(fp == NULL)
		return max + 1;
	fseek(fp, -size, SEEK_END);
	if(fread(&u, size, 1, fp) > 0)
		max = u.id;
	fclose(fp);
	return max + 1;
}

// CASE-6

void generate_order(){
	order_t rec;
	FILE *fp;
	order_accept(&rec);
	rec.id = get_next_order_id();
	fp = fopen(ORDER_DB, "ab");
	if(fp == NULL) {
		perror("order file cannot be opened");
		exit(1);
	}
	fwrite(&rec, sizeof(order_t), 1, fp);
	fclose(fp);
}

//  case-7
void display_order_report(){
	FILE *fp;
    order_t u;
	fp = fopen(ORDER_DB, "rb");
	if(fp == NULL)
	{
		perror("Cannot open file \n");
		exit(0);
	}
	// read contents from folder
	while(fread(&u, sizeof(order_t), 1, fp) > 0)
	 {
		order_display(&u);
	 }
	fclose(fp);
}


int main( void )
{
      char name[30];
	  int id;
	  int result;
      int choice;
    do{
         printf("\n\n0.Sign out\n1. Add Product\n2. find product\n3. Display all\n4. Edit product\n5. delete operation\n6. Generate Order \n7.Order Report\n Enter choice : ");
         scanf("%d",&choice);
         switch(choice)
          {
             case 1:
                add_product();
                break;

             case 2:
			    printf("Enter product name: ");
				scanf("%s", name);
				product_find_by_name(name);
                break;

             case 3:
			    display_all_product();
                break;

             case 4:
			    edit_product_by_id();
                break;

             case 5: 
			     delete_operation();
			    break;
			 
			 case 6:
			     generate_order();
                break;
			 case 7:
			     display_order_report();
           }
     }while(choice!=0);

   return 0;
}