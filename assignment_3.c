#include<stdio.h>

int menu_Choice(void)
{
	int ch;
	printf("0. Exit\n1. Enter elements\n2. Delete Number\n3. Maximum Number\n4. Minimum Number\n5. Sum\n6. Print elements\n");
	printf("Enter choice	:	");
	scanf("%d",&ch);
	return ch;
}

void add_Num(int *ptr)
{
	int num,index,i=0;
	printf("Enter number to be added	:	");
	scanf("%d",&num);
	printf("Available positions are	:	");
	for(i=0;i<10;i++)
		{
			if(ptr[i]==0)
			printf("%d\t",i+1);
		}
	printf("\nSelect index at which number is to be added	:	");
	scanf("%d",&index);
	if(ptr[index-1]==0)
			ptr[index-1]=num;
	else
			printf("Invalid index\n");

}

void delete_Num(int *ptr)
{
	int i=0,pos;
	printf("Index\t\t\tElements\n");
	for(i=0;i<10;i++)
	{
		if(ptr[i]!=0)
		{
			printf("%d\t\t\t%d\n",(i+1),ptr[i]);
		}
	}
	printf("Enter the position of corresponding number to be deleted :	");
	scanf("%d",&pos);
	if(pos!=0)
		ptr[pos-1]=0;
	else
		printf("Invalid position entered\n");
}

void max_Num(int *ptr)
{
	int num,pos;
	num=ptr[0];
	for(int i=0;i<10;i++)
	{
		if(ptr[i]>num)
		{
			num=ptr[i];
			pos=i+1;
		}
	}
	if(num!=0)
		printf("Maximum Number is %d at position %d\n",num,pos);
	else
		printf("Empty Array\n");
}

void min_Num(int *ptr)
{
	int pos,num,i=0,count=0;
	for(int n=0;n<10;n++)
	{
		if(ptr[n]!=0)
			count+=1;
	}

	if(count==0)
	{
		printf("Empty Array\n");
	}
	else if(count>1)
	{
	for(int i=0;i<10;i++)
	{
		for(int j=0;j<10;j++)
			{
				if(ptr[j]!=0)
				{
					num=ptr[j];
					pos=j+1;
					break;
				}
			}
		for(i=1;i<10;i++)
		{
			if(ptr[i]<num)
			{
				if(ptr[i]!=0)
				{
					num=ptr[i];
					pos=i+1;
				}
			}
		}
		printf("Minimum number is %d at position %d\n",num,pos);
		}
	}
	else if(count==1)
	{
		for(int i=0;i<10;i++)
			{
				if(ptr[i]!=0)
				{
					num=ptr[i];
					pos=i+1;
				}
			}
			printf("Minimum number is %d at position %d\n",num,pos);
		}
	}

void sum(int *ptr)
{
	int sum,num1,num2;
	printf("Index\t\t\tElements\n");
	for (int index=0;index<10;index++)
	{
		printf("%d\t\t\t%d\n",(index+1),ptr[index]);
	}
	printf("Enter positions of elements which are to be added	:	");
	scanf("%d %d",&num1,&num2);
	if(num1<10 && num2<10)
	{
		sum=ptr[num1-1]+ptr[num2-1];
		printf("Sum is %d\n",sum);
	}
	else
		printf("Invalid position entered\n");
}

void print_Record (int *ptr)
{
	printf("Elements are	:\n");
	printf("Index\t\t\tElements\n");
	for (int index=0;index<10;index++)
	{
		printf("%d\t\t\t%d\n",(index+1),ptr[index]);
	}
}

int main(void)
{
	
	int arr[10],ch;
	for(int i=0;i<10;i++)
		arr[i]=0;
	while((ch=menu_Choice())!=0)
	{
		switch(ch)
		{
			case 1:
				add_Num(arr);
				break;
			case 2:
				delete_Num(arr);
				break;
			case 3:
				max_Num(arr);
				break;
			case 4:
				min_Num(arr);
				break;
			case 5:
				sum(arr);
				break;
			case 6:
				print_Record(arr);
				break;
		}
	}
}
