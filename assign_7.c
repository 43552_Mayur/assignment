
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct acc_holder
{
	char name[20];
	char address[50];
	int no;

};
typedef struct account
{
	int id;
	int balance;
	char type[20];
	struct acc_holder other_info;
	struct account *next;
	struct account *prev;
}NODE;

NODE *head = NULL;

NODE* create_node()
{
	NODE *newnode;
	newnode = (NODE*)malloc(sizeof(NODE));
	printf("\n Enter account ID	:	");
	scanf("%d",&newnode->id);
	printf(" Enter account balance	:	");
	scanf("%d",&newnode->balance);
	printf(" Enter account type	:	");
	scanf("%s",newnode->type);
	printf(" Enter account holder's name	:	");
	scanf("%*c%[^\n]s",newnode->other_info.name);
	printf(" Enter account holder's address	:	");
	scanf("%*c%[^\n]s",newnode->other_info.address);
	printf(" Enter account holder's contact number	:	");
	scanf("%d",&newnode->other_info.no);
	newnode->next = NULL;
	newnode->prev = NULL;
	return newnode;

}

void addatfirst()
{
	NODE *newnode;
	newnode = create_node();
	if (head == NULL)
	{
		head = newnode;
	}
	else
	{
		newnode->next = head;
		head->prev = newnode;
		head = newnode;
	}
	printf("\nRECORD SUCCESSFUL\n");

}

void addatlast()
{
	NODE *newnode;
	NODE *trav;
	newnode = create_node();
	if (head == NULL)
	{
		head = newnode;
	}
	else
	{
		trav = head;
		while(trav->next != NULL)
		{
			trav = trav->next;
		}
		trav->next = newnode;
		newnode->prev = trav;
	}
	printf("\nRECORD SUCCESSFUL\n");
}

void display_fwd()
{
	NODE *trav;
	if (head == NULL)
	{
		printf("\nLIST EMPTY");
		return;
	}
	trav = head;
	while (trav != NULL)
	{
		printf("\n \n Account ID 				:	%d",trav->id);
		printf("\n Account balance				:	%d",trav->balance);
		printf("\n Account type					:	%s",trav->type);
		printf("\n Account holder's name		:	%s",trav->other_info.name);
		printf("\n Account holder's address		:	%s",trav->other_info.address);
		printf("\n Account holder's contact number	:	%d",trav->other_info.no);
		trav = trav->next;
	}
}
void display_rev()
{
	NODE *trav;
	if (head == NULL)
	{
		printf("\n EMPTY LIST");
		return;
	}
	trav = head;
	while (trav->next != NULL)
	{
		trav = trav->next;
	}
	while (trav != NULL)
	{
		printf("\n \n Account ID			:	%d",trav->id);
		printf("\n Account balance			:	%d",trav->balance);
		printf("\n Account type				:	%s",trav->type);
		printf("\n Account holder's name	:	%s",trav->other_info.name);
		printf("\n Account holder's			:	%s",trav->other_info.address);
		printf("\n Account holder's contact number	:	%d",trav->other_info.no);
		trav = trav->prev;
	}
}

void delete_all()
{
	NODE *temp;
	while (head != NULL)
	{
		temp = head;
		head = head->next;
		free(temp);
	}
}

void id_search()
{
	if (head == NULL)
	{
		printf("\n LIST EMPTY");
		return;
	}
	NODE *trav;
	trav = head;
	int id;
	printf("Enter Account ID whose data is to be fetched	:	");
	scanf("%d",&id);
	while (trav != NULL)
	{
		if (trav->id == id)
		{
			printf("\n \n Account ID = %d",trav->id);
			printf("\n Account balance = %d",trav->balance);
			printf("\n Account type is %s",trav->type);
			printf("\n Account holder's name is %s",trav->other_info.name);
			printf("\n Account holder's address is %s",trav->other_info.address);
			printf("\n Account holder's contact number is %s",trav->other_info.no);
			return;
		}
		trav = trav->next;
	}
	printf("Record not exists\n");
}

void name_search()
{
	if (head == NULL)
	{
		printf("\nEMPTY LIST");
		return;
	}
	NODE *trav;
	trav = head;
	char name[20];
	printf("Enter Account name whose data is to be fetched	:	");
	scanf("%*c%[^\n]s",name);
	while (trav != NULL)
	{
		if (strcmp(trav->other_info.name , name) == 0)
		{
			printf("\n \n Account ID = %d",trav->id);
			printf("\n Account balance = %d",trav->balance);
			printf("\n Account type is %s",trav->type);
			printf("\n Account holder's name is %s",trav->other_info.name);
			printf("\n Account holder's address is %s",trav->other_info.address);
			printf("\n Account holder's contact number is %d",trav->other_info.no);
			return;
		}
		trav = trav->next;
	}
	printf("Data doesn't exists\n");
}
int menu_list(void)
{
	int ch;
	printf("\n 0. Exit");
	printf("\n 1. Add at first position");
	printf("\n 2. Add at last position");
	printf("\n 3. Display(Forward)");
	printf("\n 4. Display(Reverse)");
	printf("\n 5. Delete all record");
	printf("\n 6. Search account by ID");
	printf("\n 7. Search account by account holder's name");
	printf("\n Enter choice	:	");
	scanf("%d",&ch);
	return ch;
}

int main(void)
{
	setvbuf(stdout,NULL,_IONBF,0);
	int ch;
	while((ch=menu_list())!=0)
	{
		switch(ch)
		{
		case 1 :
			addatfirst();
			break;

		case 2 :
			addatlast();
			break;

		case 3 :
			display_fwd();
			break;

		case 4 :
			display_rev();
			break;

		case 5 :
			delete_all();
			break;

		case 6 :
			id_search();
			break;

		case 7:
			name_search();
			break;
		default:
			printf("Invalid Choice\n");
		}
	};
	return 0;
}


