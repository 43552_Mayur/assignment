#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define SIZE 20
struct book
{
	int id;
	char name[30];
	int price;
};
void accept_record(struct book *ptr);
void print_record(struct book *ptr,int n);
void merge_sort(struct book a[],int beg,int end);
void merge(struct book a[],int beg,int mid,int end);
int menu_choice(void)
{
	int ch;
	printf("0. Exit\n");
	printf("1. Accept record\n");
	printf("2. Show record\n");
	printf("3. Show record( on the basis of price )\n");
	printf("4. Show record( on the basis of name )\n");
	printf("Enter your choice	:	");
	scanf("%d",&ch);
	return ch;
}
int cmpf(const void *a,const void *b)
	{
		return strcmp(((struct book*)a)->name,((struct book*)b)->name);
	}
int main(void)
{
	setvbuf(stdout,NULL,_IONBF,0);
	int no,ch;
	printf("Enter the number of books	:	");
	scanf("%d",&no);
	struct book b[SIZE];
	while((ch=menu_choice())!=0)
		{
			switch(ch)
			{
			case 1:
				for(int i=0;i<no;i++)
					accept_record(&b[i]);
				break;
			case 2:
				printf("Book no.\tID\t\t\t\tName\t\t\t\t\tPrice\n");
				for(int i=0;i<no;i++)
					print_record(&b[i],i);
				break;
			case 3:
				merge_sort(b,0,no-1);
				printf("Sorted array on the basis of price is :	\n");
				printf("Book no.\tID\t\t\t\tName\t\t\t\t\tPrice\n");
				for(int i=0;i<no;i++)
					print_record(&b[i],i);
				break;
			case 4:
				qsort(b,no,sizeof(struct book),cmpf);
				printf("Sorted array on the basis of price is :	\n");
				printf("Book no.\tID\t\t\t\tName\t\t\t\t\tPrice\n");
				for(int i=0;i<no;i++)
					print_record(&b[i],i);
				break;
			default:
				printf("Invalid choice");
				break;
			}
		}
}
void merge(struct book b[],int beg,int mid,int end)
{
	struct book temp[SIZE];
	int i=beg, j=mid+1, index=beg, k;
	while((i<=mid) && (j<=end))
	{
		if( b[i].price > b[j].price )
		{
			temp[index]=b[i];
			i++;
		}
		else
		{
			temp[index]=b[j];
			j++;
		}
		index++;
	}
	if(i>mid)
			{
				while(j<=end)
				{
					temp[index]=b[j];
					j++;
					index++;
				}
			}
			else
			{
				while(i<=mid)
				{
					temp[index]=b[i];
					i++;
					index++;
				}
			}
	for(k=beg;k<index;k++)
		b[k] = temp[k];
}
void merge_sort(struct book b[],int beg,int end)
{
	int mid;
	if(beg<end)
	{
		mid = (beg+end)/2;
		merge_sort(b,beg,mid);
		merge_sort(b,mid+1,end);
		merge(b,beg,mid,end);
	}
}
void accept_record(struct book *ptr)
{
	printf("Enter Book ID		:	");
	scanf("%d",&ptr->id);
	printf("Enter Book Name		:	");
	scanf("%s",ptr->name);
	printf("Enter Book price	:	");
	scanf("%d",&ptr->price);
}
void print_record(struct book *ptr,int n)
{
	printf("%d\t\t%d\t\t\t\t%s\t\t\t\t\t%d\n",n+1,ptr->id,ptr->name,ptr->price);
}
